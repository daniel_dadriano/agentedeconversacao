
package Func;

public class AIML {
    
    public static String criaAIML (){
        
       String aiml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n<aiml>\n\n</aiml>";
       
       return aiml;
      
    }       
    
    public static String criaCategory (String pattern, String template){
        
        String category = "\n\n<category>"+"\n      "+pattern+"\n       "+template+"\n"+"</category>";
        
        return category;
    }
    
    public static String criaPattern (String conteudo){
        
        String pattern = "<pattern>"+conteudo+"</pattern>";
        
        return pattern;
    }

    public static String criaTemplate (String conteudo, int num){
        
        String template = null;
                
        if (num == 1){
            template = "<template>"+conteudo+"</template>";
        }
        
        if (num == 2){
            template = "<template><srai>"+conteudo+"</srai></template>";
        }
        
        if (num == 3){
            template = "<template>\n            <random>\n"+conteudo+
                    "            </random>\n      </template>";
        }
                
        return template;
    }
    
    public static String criaLi (String conteudo){
        
        String li = "               <li>"+conteudo+"</li>\n";
        
        return li;
    }
    
    public static String iniciarAIML(){
       
        String aiml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n<aiml>\n";
       
        return aiml; 
    }
    
    public static String finalizarAIML(){
        String aiml = "\n</aiml>";
        
        return  aiml;
    }
    
}