/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Func;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Daniel
 */
public class GerarArquivoResposta {

    private List<String> listaPerguntas = null;
    private List<String> arquivoAiml = null;

    public GerarArquivoResposta(List<String> lista) {
        this.listaPerguntas = lista;
        this.arquivoAiml = new ArrayList<String>();
    }
                   
    public List<String> gerarArquivoAiml(String palavraChaveSingular, String palavraChavePlural, String resposta){
                     
        String patternPrincipal = "O QUE E " + palavraChaveSingular;
        
        arquivoAiml.add(AIML.iniciarAIML());
        arquivoAiml.add(AIML.criaCategory(AIML.criaPattern(patternPrincipal),AIML.criaTemplate(resposta, 1)));
        
        for (int i = 0; i < listaPerguntas.size(); i++) {
            
            String pattern;
            pattern = listaPerguntas.get(i);
            
            if (pattern.equals("") == false){
                
                if (pattern.contains("<PALAVRA CHAVE SINGULAR>")){
                    pattern = pattern.replace("<PALAVRA CHAVE SINGULAR>", palavraChaveSingular);
                }else{
                    pattern = pattern.replace("<PALAVRA CHAVE PLURAL>", palavraChavePlural);
                }                  
                                                   
                arquivoAiml.add(AIML.criaCategory(AIML.criaPattern(pattern), AIML.criaTemplate(patternPrincipal, 2)));
                
            }
                        
        }
        
        arquivoAiml.add(AIML.finalizarAIML());
                       
        return arquivoAiml;
    }
    
}
