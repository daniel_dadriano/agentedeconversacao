package Func;

import java.util.Arrays;  
  
class main  
{  
   public static void main (String s[])  
   {  
      //Construindo Array  
      String vetor[] = new String[11];  
        
      //Inserção de caracteres  
      vetor[0] = "Abacate a";  
      vetor[1] = "Melancia";  
      vetor[2] = "Tomate";  
      vetor[3] = "Abacaxi";  
      vetor[4] = "Melão";  
      vetor[5] = "Banana";  
      vetor[6] = "Laranja";  
      vetor[7] = "Caju";  
      vetor[8] = "Carambola";  
      vetor[9] = "Mamão";
      vetor[10] = "Abacate b";
        
      //Ordenação  
      Arrays.sort(vetor);  
        
      //Impressão  
      for (int i = 0 ; i<vetor.length;i++)  
      {  
         System.out.print (vetor[i]+", ");  
      }  
      System.out.println();  
   }     
}  

