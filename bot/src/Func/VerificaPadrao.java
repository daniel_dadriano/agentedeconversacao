
package Func;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class VerificaPadrao {
        
    public static ArrayList VerificaPadrao (String conteudo, String pattern){
        ArrayList<Integer> valores = new ArrayList<Integer>();
        
        Matcher matcher = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE).matcher(
			conteudo);

        while (matcher.find()) {
            valores.add(matcher.start());  // posicao em que se encontra o dado.
            valores.add(matcher.end());    // posicao final do dado
        }
        return valores;
    }


    public static String FiltraPattern (String conteudo){
        String s = conteudo;  

        int begin = s.indexOf("<pattern>");  
        int end = s.lastIndexOf("</pattern>");  

        String filtrada = s.substring(begin + 9, end);  

        return filtrada;
    }
}