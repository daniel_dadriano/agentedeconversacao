package Func;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;
import telas.Categorias;

/**
 * @author narcizo
 * Retorna a posição em que está localizada a última linha do arquivo para que se possa
 * saber onde serão inseridas as categorias.
 */

public class LocUltimaLinha {

    public static int LocUltimaLinha () {
        JTextArea ta = Categorias.taTextoArquivo;
        int ultimaLinha = 0;

        try {
          for (int n = 0; n < ta.getLineCount(); n += 1){
            ultimaLinha = ta.getLineStartOffset(n);
          }

        } catch (BadLocationException ex) {
          System.out.println(ex);
        }
        return ultimaLinha;
    }
}