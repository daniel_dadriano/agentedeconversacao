
package Func;

/**
 * @author narcizo
 * Pega o diretorio completo do arquivo gerado pelo JFileChooser e extrai apenas o nome do arquivo.
 */

public class NomeArquivo {
        
    public static String NomeArquivo (String caminho){
        String nome = "";
        int i;
        
        int length = caminho.length();

        for (i=length-1; i >= 0; i--){
            if (caminho.charAt(i) != '\\' ){
                 nome = nome + caminho.charAt(i) ;
            }
            else{
                break;
            }
        }
        StringBuffer nomeArquivo = new StringBuffer(nome); 
        nomeArquivo.reverse(); 
        String str = nomeArquivo.toString();
        
        return str;
    }
}
