
package Arquivo;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.print.DocFlavor;
import javax.swing.JFileChooser;
import javax.swing.UIManager;


public class Arquivo {
    
    public static void traduzFileChooser (){
        
        UIManager.put("FileChooser.openDialogTitleText", "Abrir");
        UIManager.put("FileChooser.saveDialogTitleText", "Salvar");
        UIManager.put("FileChooser.lookInLabelMnemonic", "E");  
        UIManager.put("FileChooser.lookInLabelText", "Examinar em");  
        UIManager.put("FileChooser.saveInLabelMnemonic", "S");  
        UIManager.put("FileChooser.saveInLabelText", "Salvar em");  
        UIManager.put("FileChooser.upFolderToolTipText", "Um nível acima");  
        UIManager.put("FileChooser.upFolderAccessibleName", "Um nível acima");  
        UIManager.put("FileChooser.homeFolderToolTipText", "Desktop");  
        UIManager.put("FileChooser.homeFolderAccessibleName", "Desktop");  
        UIManager.put("FileChooser.newFolderToolTipText", "Criar nova pasta");  
        UIManager.put("FileChooser.newFolderAccessibleName", "Criar nova pasta");  
        UIManager.put("FileChooser.listViewButtonToolTipText", "Lista");  
        UIManager.put("FileChooser.listViewButtonAccessibleName", "Lista");  
        UIManager.put("FileChooser.detailsViewButtonToolTipText", "Detalhes");  
        UIManager.put("FileChooser.detailsViewButtonAccessibleName", "Detalhes");  
        UIManager.put("FileChooser.fileNameLabelMnemonic", "N");  
        UIManager.put("FileChooser.fileNameLabelText", "Nome do arquivo");  
        UIManager.put("FileChooser.filesOfTypeLabelMnemonic", "A");  
        UIManager.put("FileChooser.filesOfTypeLabelText", "Arquivos do tipo");  
        UIManager.put("FileChooser.fileNameHeaderText", "Nome");  
        UIManager.put("FileChooser.fileSizeHeaderText", "Tamanho");  
        UIManager.put("FileChooser.fileTypeHeaderText", "Tipo");  
        UIManager.put("FileChooser.fileDateHeaderText", "Data");  
        UIManager.put("FileChooser.fileAttrHeaderText", "Atributos");  
        UIManager.put("FileChooser.cancelButtonText", "Cancelar");  
        UIManager.put("FileChooser.cancelButtonMnemonic", "C");  
        UIManager.put("FileChooser.cancelButtonToolTipText", "Cancelar");  
        UIManager.put("FileChooser.openButtonText", "Abrir");  
        UIManager.put("FileChooser.openButtonMnemonic", "A");  
        UIManager.put("FileChooser.openButtonToolTipText", "Abrir");  
        UIManager.put("FileChooser.saveButtonText", "Salvar");  
        UIManager.put("FileChooser.saveButtonToolTipText", "S");  
        UIManager.put("FileChooser.saveButtonToolTipText", "Salvar");  
        UIManager.put("FileChooser.updateButtonText", "Alterar");  
        UIManager.put("FileChooser.updateButtonToolTipText", "A");  
        UIManager.put("FileChooser.updateButtonToolTipText", "Alterar");  
        UIManager.put("FileChooser.helpButtonText", "Ajuda");  
        UIManager.put("FileChooser.helpButtonToolTipText", "A");  
        UIManager.put("FileChooser.helpButtonToolTipText", "Ajuda");  
        UIManager.put("FileChooser.acceptAllFileFilterText", "Todos os arquivos");
    }
    
    public static void traduzJOptionPane (){
        UIManager.put("OptionPane.cancelButtonText", "Cancelar");  
        UIManager.put("OptionPane.noButtonText", "Não");  
        UIManager.put("OptionPane.yesButtonText", "Sim");  
    }
    
    public static String abrirFileChooser(){
        
        traduzFileChooser();
        
        JFileChooser arquivo = new JFileChooser();
        String caminho = "";
        
        int retorno = arquivo.showOpenDialog(null);
        if (retorno == JFileChooser.APPROVE_OPTION){
            caminho = arquivo.getSelectedFile().getAbsolutePath();
            return caminho;
        }else{
            System.out.println("Não deu certo");
        }
        return null;
    }
    
    public static String salvarFileChooser() {
        
        traduzFileChooser();
        
        JFileChooser arquivo = new JFileChooser();
        String caminho = "";

        int retorno = arquivo.showSaveDialog(null); // showSaveDialog retorna um inteiro , e ele ira determinar que o chooser será para salvar.
        if (retorno == JFileChooser.APPROVE_OPTION){
            caminho = arquivo.getSelectedFile().getAbsolutePath();  // o getSelectedFile pega o arquivo e o getAbsolutePath retorna uma string contendo o endereço.         
        }    
        
        if(!caminho.equals("")){
            return caminho+".aiml";
        }else{
            System.out.println("Não deu certo");
        }
        return null;
    }

    public static void salvar(String arquivo, String conteudo, boolean adicionar) throws IOException {
        FileWriter fw = new FileWriter(arquivo, adicionar);
        fw.write(conteudo);
        fw.close();
    }
    
    /**
    * Carrega o conteúdo de um arquivo em uma String, se o aquivo
    * não existir, retornará null.
    * @param arquivo
    * @return conteúdo
    * @throws Exception
    */
    public static String ler(String arquivo) throws FileNotFoundException, IOException {
        File file = new File(arquivo);

        if (! file.exists()) {
            return null;
        }
    
        BufferedReader br = new BufferedReader(new FileReader(arquivo));
        StringBuffer bufSaida = new StringBuffer();
        String linha;

        while( (linha = br.readLine()) != null ){
            bufSaida.append(linha + "\n");
        }
        br.close();
        return bufSaida.toString();
    }
    
    public static List<String> lerArquivo(String arquivo) throws FileNotFoundException, IOException{
        
        List<String> listaArquivo = new ArrayList<String>();
        
        FileReader fileReader = new FileReader(arquivo);
        BufferedReader lerArquivo = new BufferedReader(fileReader);
                       
        String linha = lerArquivo.readLine();
                       
        while (linha != null){
            listaArquivo.add(linha);
            linha = lerArquivo.readLine();
        }
                               
        return listaArquivo;
        

    }

}