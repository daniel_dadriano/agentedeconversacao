
package telas;

import Arquivo.Arquivo;
import Func.AIML;
import Func.LocUltimaLinha;
import Func.VerificaPadrao;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Categorias extends javax.swing.JPanel {
    
    private static Integer CADASTRO_MANUAL = 0;
    private static Integer CADASTRO_AUTOMOATICO = 1;
    
    private Integer tipoCadastro;
    
    static String manual = "manual";
    static String automatica = "automatica";
                 
    public Categorias() {
        initComponents();     
        inicializaCamadasDePaineis();
        fechaArquivo();
    }
    
    private void inicializaCamadasDePaineis(){
        cardLayout = new CardLayout();
        painelBase.setLayout (cardLayout);
        painel1 = new CatManual();
        painel2 = new CatAutomatica();
        
        painelBase.add (painel1, "manual");
        painelBase.add (painel2, "automatica");
        
	jRadioButton1.setActionCommand(manual);
	jRadioButton2.setActionCommand(automatica);

        jRadioButton1.setSelected(true);
	
        // Group the radio buttons.
	buttonGroup1.add(jRadioButton1);
	buttonGroup1.add(jRadioButton2);

        // Register a listener for the radio buttons.
	RadioListener myListener = new RadioListener();
	jRadioButton1.addActionListener(myListener);
	jRadioButton2.addActionListener(myListener);
        
	add(jRadioButton1);
	add(jRadioButton2);
    }
    
    public static void fechaArquivo() {
        caminhoDoArquivo.setText("");
        taTextoArquivo.setText("");
        taTextoArquivo.setEnabled(false);
        CatManual.CampoPattern.setText("");
        CatManual.CampoPattern.setEnabled(false);
        CatManual.CampoTemplate.setText("");
        CatManual.CampoTemplate.setEnabled(false);
        CatManual.jButton1.setEnabled(false);
        CatManual.jButton2.setEnabled(false);
        CatManual.deletaLista();
        CatManual.jList2.setEnabled(false);
        QtdCategorias.setText("");
        BotaoFechar.setEnabled(false);
        BotaoGerar.setEnabled(false);
        BotaoLimpar.setEnabled(false);
        CatManual.CheckBoxSrai.setSelected(false);
        CatManual.CheckBoxSrai.setEnabled(false);
        CatManual.ComboBoxPatterns.removeAllItems();
        CatManual.ComboBoxPatterns.addItem("Selecione o pattern");
        CatManual.ComboBoxPatterns.setEnabled(false);
        TelaPrincipal.Salvar.setEnabled(false);
    }
       
    public static void abreArquivo() {
        BotaoFechar.setEnabled(true);
        BotaoGerar.setEnabled(true);
        BotaoLimpar.setEnabled(true);
        taTextoArquivo.setEnabled(true);
        CatManual.CampoPattern.setEnabled(true);
        CatManual.CampoTemplate.setEnabled(true);
        CatManual.jButton1.setEnabled(true);
        CatManual.jButton2.setEnabled(true);
        CatManual.jList2.setEnabled(true);
        CatManual.CheckBoxSrai.setEnabled(true);
        CatManual.ComboBoxPatterns.removeAllItems();
        CatManual.ComboBoxPatterns.addItem("Selecione o pattern");
        CatManual.ComboBoxPatterns.setEnabled(false);
        TelaPrincipal.Salvar.setEnabled(true);
    }

    private void LimparCampos(){
        CatManual.CampoPattern.setText("");
        CatManual.CampoTemplate.setText("");
        CatManual.deletaLista();
        CatManual.CheckBoxSrai.setSelected(false);
        CatManual.ComboBoxPatterns.addItem("Selecione o pattern");
    }

    class RadioListener implements ActionListener{
	public void actionPerformed(ActionEvent e) {
	    String factoryName = null;
            
	    if (e.getActionCommand().equals(manual)) {
                cardLayout.show (painelBase, "manual");
                tipoCadastro = CADASTRO_MANUAL;
	    } else {
                cardLayout.show (painelBase, "automatica");
                tipoCadastro = CADASTRO_AUTOMOATICO;
	    }
	}
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        taTextoArquivo = new javax.swing.JTextArea();
        QtdCategorias = new javax.swing.JLabel();
        caminhoDoArquivo = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        painelBase = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jRadioButton2 = new javax.swing.JRadioButton();
        jRadioButton1 = new javax.swing.JRadioButton();
        jPanel5 = new javax.swing.JPanel();
        BotaoGerar = new javax.swing.JButton();
        BotaoLimpar = new javax.swing.JButton();
        BotaoFechar = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(888, 460));

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        taTextoArquivo.setColumns(20);
        taTextoArquivo.setRows(5);
        jScrollPane1.setViewportView(taTextoArquivo);

        QtdCategorias.setText("qtd categorias");

        caminhoDoArquivo.setText("caminho do arquivo");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(caminhoDoArquivo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 271, Short.MAX_VALUE)
                .addComponent(QtdCategorias)
                .addContainerGap())
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(QtdCategorias)
                    .addComponent(caminhoDoArquivo)))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setMaximumSize(new java.awt.Dimension(411, 500));

        javax.swing.GroupLayout painelBaseLayout = new javax.swing.GroupLayout(painelBase);
        painelBase.setLayout(painelBaseLayout);
        painelBaseLayout.setHorizontalGroup(
            painelBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        painelBaseLayout.setVerticalGroup(
            painelBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 280, Short.MAX_VALUE)
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Tipo de cadastro das categorias"));

        jRadioButton2.setText("Automático");
        jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });

        jRadioButton1.setText("Manual");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jRadioButton1)
                .addGap(67, 67, 67)
                .addComponent(jRadioButton2)
                .addContainerGap(173, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton1)
                    .addComponent(jRadioButton2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Ações"));

        BotaoGerar.setText("Gerar");
        BotaoGerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotaoGerarActionPerformed(evt);
            }
        });

        BotaoLimpar.setText("Limpar");
        BotaoLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotaoLimparActionPerformed(evt);
            }
        });

        BotaoFechar.setText("Fechar");
        BotaoFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotaoFecharActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(BotaoGerar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BotaoLimpar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BotaoFechar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BotaoGerar)
                    .addComponent(BotaoLimpar)
                    .addComponent(BotaoFechar))
                .addGap(0, 11, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(painelBase, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelBase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(22, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    private void BotaoLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaoLimparActionPerformed
        CatManual.CampoPattern.setText("");
        CatManual.CampoTemplate.setText("");
    }//GEN-LAST:event_BotaoLimparActionPerformed
   
    private boolean validaPreenchimentoCampos (){
        boolean teste = false;

            if(CatManual.CampoPattern.getText().equals(""))
            {
            JOptionPane.showMessageDialog(null, "Você deve preencher o campo Pattern");
            }
            else if (CatManual.CheckBoxSrai.isSelected())
            {
                if ("Selecione o pattern".equals(CatManual.ComboBoxPatterns.getSelectedItem().toString()))
                {
                    JOptionPane.showMessageDialog(null, "VocSelecione o Srai");
                }
                else
                {
                    teste = true;
                }
            }
            else if (CatManual.CampoTemplate.getText().equals("") && CatManual.jList2.getModel().getSize() == 0)
            {
                JOptionPane.showMessageDialog(null, "Você deve preencher ao menos uma resposta ou selecionar a opção \"Direcionar para pattern existente\"");
            }
            else {
                teste = true;
            }
        return teste;
    }

    private void cadastroManual(){
        
        if (validaPreenchimentoCampos())
        {
            ArrayList<Integer> patterns;
            ArrayList<Integer> categorias;
            String pattern = AIML.criaPattern(CatManual.CampoPattern.getText());
            String template = "";

            if (CatManual.CheckBoxSrai.isSelected()){
                String textoCombobox = CatManual.ComboBoxPatterns.getSelectedItem().toString();
                if("Selecione o pattern".equals(textoCombobox))
                {
                    JOptionPane.showMessageDialog(null, "Selecione o pattern");
                }
                else{
                    template = AIML.criaTemplate(textoCombobox, 2);
                    CatManual.ComboBoxPatterns.setSelectedItem("Selecione o pattern");
                }
            }
            else{
                if(!CatManual.CampoTemplate.getText().equals(""))
                {
                    template = AIML.criaTemplate(CatManual.CampoTemplate.getText(), 1);
                }
                else{
                    String li = "";
                    int qtdElementos = CatManual.jList2.getModel().getSize();

                    for(int i = 0; i < qtdElementos; i++)
                    {
                        li += AIML.criaLi(CatManual.jList2.getModel().getElementAt(i).toString());
                    }
                    template = AIML.criaTemplate(li, 3);
                }
            }

            String category = AIML.criaCategory(pattern, template);

            String conteudo = taTextoArquivo.getText();
            patterns = Func.VerificaPadrao.VerificaPadrao(conteudo, pattern);
            if ( !patterns.isEmpty()){
                JOptionPane.showMessageDialog(null, "Esse pattern já existe.");
                taTextoArquivo.requestFocus();
                taTextoArquivo.setCaretPosition(patterns.get(0));
            }
            else{
                taTextoArquivo.insert(category, LocUltimaLinha.LocUltimaLinha() - 10);
                pattern = CatManual.CampoPattern.getText();
                CatManual.ComboBoxPatterns.addItem(pattern);

                // VERIFICA QUANTAS CATEGORIAS O ARQUIVO POSSUI
                conteudo = taTextoArquivo.getText();
                categorias = VerificaPadrao.VerificaPadrao(conteudo, "<category>");
                QtdCategorias.setText(categorias.size()/2+" Categorias");

                LimparCampos();
            }
        }
    }
    
    private void BotaoGerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaoGerarActionPerformed

        if (tipoCadastro == CADASTRO_MANUAL){
            cadastroManual();
        }
    }//GEN-LAST:event_BotaoGerarActionPerformed

    private void BotaoFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaoFecharActionPerformed
        Arquivo.traduzJOptionPane();

        int resposta = JOptionPane.showConfirmDialog(null, "Deseja fechar o arquivo "+caminhoDoArquivo.getText());
        if (resposta == JOptionPane.YES_OPTION){
            fechaArquivo();
        }
    }//GEN-LAST:event_BotaoFecharActionPerformed

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton2ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton BotaoFechar;
    public static javax.swing.JButton BotaoGerar;
    public static javax.swing.JButton BotaoLimpar;
    public static javax.swing.JLabel QtdCategorias;
    private javax.swing.ButtonGroup buttonGroup1;
    public static javax.swing.JLabel caminhoDoArquivo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel painelBase;
    public static javax.swing.JTextArea taTextoArquivo;
    // End of variables declaration//GEN-END:variables
    private CardLayout cardLayout;
    private JPanel painel1;
    private JPanel painel2;
}