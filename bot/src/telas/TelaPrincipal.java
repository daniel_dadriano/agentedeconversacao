
package telas;

import Arquivo.Arquivo;
import Func.AIML;
import Func.NomeArquivo;
import Func.VerificaPadrao;
import java.awt.CardLayout;
import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class TelaPrincipal extends javax.swing.JFrame {

    public TelaPrincipal() {
        initComponents();
        inicializaCardLayout();
    }

    public void inicializaCardLayout (){
        cardLayout = new CardLayout();
        PainelPrincipal.setLayout (cardLayout);

        PainelDefault = new PainelDefault();
        Painel1 = new Categorias();
        Painel2 = new Perguntas();
        Painel3 = new Respostas();
        Painel4 = new Importar();
        Painel5 = new Testar();
        Painel6 = new Busca();

        PainelPrincipal.add (PainelDefault, "PainelDefault");
        PainelPrincipal.add (Painel1, "Painel1");
        PainelPrincipal.add (Painel2, "Painel2");
        PainelPrincipal.add (Painel3, "Painel3");
        PainelPrincipal.add (Painel4, "Painel4");
        PainelPrincipal.add (Painel5, "Painel5");
        PainelPrincipal.add (Painel6, "Painel6");

        trocaPainel(1);
    }


    public static void trocaPainel (int num)
    {
//        cardLayout = new CardLayout();
        cardLayout.show(PainelPrincipal, "Painel"+num);
    }

    public void inicia (){
        trocaPainel(1);
        Categorias.BotaoFechar.setEnabled(true);
        Categorias.abreArquivo();
    }

    public void le (String caminho){
        String str;

        try {
            str = Arquivo.ler(caminho);
            Categorias.taTextoArquivo.append(str);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Categorias.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Categorias.class.getName()).log(Level.SEVERE, null, ex);
        }

        inicia();
        Categorias.caminhoDoArquivo.setText(NomeArquivo.NomeArquivo(caminho));
        Categorias.caminhoDoArquivo.setForeground(Color.gray);
        Categorias.caminhoDoArquivo.setToolTipText(caminho); 
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PainelSuperior = new javax.swing.JPanel();
        Abrir = new javax.swing.JButton();
        Novo = new javax.swing.JButton();
        TelaCategorias = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        Salvar = new javax.swing.JButton();
        PainelPrincipal = new javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenu5 = new javax.swing.JMenu();
        jMenu6 = new javax.swing.JMenu();
        jMenu7 = new javax.swing.JMenu();
        jMenu8 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gerador de Categorias para Chatterbot");
        setPreferredSize(new java.awt.Dimension(1024, 700));

        PainelSuperior.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        Abrir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/abrir.png"))); // NOI18N
        Abrir.setToolTipText("Abrir");
        Abrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AbrirActionPerformed(evt);
            }
        });

        Novo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/novo.png"))); // NOI18N
        Novo.setToolTipText("Novo");
        Novo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NovoActionPerformed(evt);
            }
        });

        TelaCategorias.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/categorias.png"))); // NOI18N
        TelaCategorias.setToolTipText("Categorias");
        TelaCategorias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TelaCategoriasActionPerformed(evt);
            }
        });

        jButton10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/Sobre.png"))); // NOI18N
        jButton10.setToolTipText("Sobre");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        Salvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/salvar.png"))); // NOI18N
        Salvar.setToolTipText("Salvar");
        Salvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalvarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PainelSuperiorLayout = new javax.swing.GroupLayout(PainelSuperior);
        PainelSuperior.setLayout(PainelSuperiorLayout);
        PainelSuperiorLayout.setHorizontalGroup(
            PainelSuperiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PainelSuperiorLayout.createSequentialGroup()
                .addComponent(Abrir, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Novo, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Salvar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TelaCategorias, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(781, Short.MAX_VALUE))
        );
        PainelSuperiorLayout.setVerticalGroup(
            PainelSuperiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Novo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(Abrir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(TelaCategorias, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jButton10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(Salvar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        PainelPrincipal.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        PainelPrincipal.setPreferredSize(new java.awt.Dimension(888, 460));

        javax.swing.GroupLayout PainelPrincipalLayout = new javax.swing.GroupLayout(PainelPrincipal);
        PainelPrincipal.setLayout(PainelPrincipalLayout);
        PainelPrincipalLayout.setHorizontalGroup(
            PainelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        PainelPrincipalLayout.setVerticalGroup(
            PainelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 473, Short.MAX_VALUE)
        );

        jMenu1.setText("Arquivo");

        jMenuItem4.setText("Novo");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem4);

        jMenuItem5.setText("Abrir");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem5);

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/fechar.png"))); // NOI18N
        jMenuItem1.setText("Sair");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Categorias");

        jMenuItem3.setText("Cadastrar Categorias");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Perguntas");

        jMenuItem7.setText("Cadastrar perguntas");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem7);

        jMenuBar1.add(jMenu3);

        jMenu4.setText("Respostas");
        jMenuBar1.add(jMenu4);

        jMenu5.setText("Importar");
        jMenuBar1.add(jMenu5);

        jMenu6.setText("Testar");
        jMenuBar1.add(jMenu6);

        jMenu7.setText("Busca");
        jMenuBar1.add(jMenu7);

        jMenu8.setText("Sobre");

        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/info.png"))); // NOI18N
        jMenuItem2.setText("Sobre");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu8.add(jMenuItem2);

        jMenuBar1.add(jMenu8);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PainelSuperior, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(PainelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, 952, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(PainelSuperior, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PainelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, 477, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        Sobre sobre = new Sobre(this, true);
        sobre.setLocationRelativeTo(null);  //abre a janela centralizada na tela
        sobre.setVisible(true);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        trocaPainel(1);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void AbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AbrirActionPerformed
        String caminho = Arquivo.abrirFileChooser();
        ArrayList<Integer> categorias;
        ArrayList<Integer> patterns;

        try {
            // CHAMA O METODO LER PARA LER O ARQUIVO, DEPOIS ADICIONA A AREA DE TEXTO.
            String str = Arquivo.ler(caminho);            
            Categorias.taTextoArquivo.append(str);
            String conteudo = Categorias.taTextoArquivo.getText();

            // VERIFICA OS PATTERNS PARA ADICIONAR A COMBOBOX
            patterns = VerificaPadrao.VerificaPadrao(conteudo, "<pattern>.+</pattern>");

            for(int i = 0; i < patterns.size(); i=i+2) {
                String texto = "";  
                for (int j = patterns.get(i); j < patterns.get(i+1); j++){                
                    texto = texto + conteudo.charAt(j);
                }
                String filtrada = Func.VerificaPadrao.FiltraPattern(texto);
                CatManual.ComboBoxPatterns.addItem(filtrada);
            }

            // VERIFICA QUANTAS CATEGORIAS O ARQUIVO POSSUI
            categorias = VerificaPadrao.VerificaPadrao(conteudo, "<category>");
            System.out.println(categorias.size());
            Categorias.QtdCategorias.setText(categorias.size()/2+" Categorias");

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Categorias.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Categorias.class.getName()).log(Level.SEVERE, null, ex);
        }

        inicia();
        Categorias.caminhoDoArquivo.setText(NomeArquivo.NomeArquivo(caminho));
        Categorias.caminhoDoArquivo.setForeground(Color.gray);
        Categorias.caminhoDoArquivo.setToolTipText(caminho);
    }//GEN-LAST:event_AbrirActionPerformed

    private void NovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NovoActionPerformed
        String caminho = Arquivo.salvarFileChooser();
        String texto = AIML.criaAIML();
        boolean exists = (new File(caminho)).exists();
        Arquivo.traduzJOptionPane();

        try{
            if (exists){
                int resposta = JOptionPane.showConfirmDialog(null, "O arquivo já existe. Deseja sobrescrevê-lo? "+Categorias.caminhoDoArquivo.getText());
                if (resposta == JOptionPane.YES_OPTION){
                    Arquivo.salvar(caminho, texto, false);
                    le(caminho);
                }
                else{
                    if (resposta == JOptionPane.NO_OPTION){
                        NovoActionPerformed(evt);
                    }
                }
            }
            else{
                Arquivo.salvar(caminho, texto, false);
                le(caminho);
            }
        } catch (IOException ex) {
            Logger.getLogger(Categorias.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_NovoActionPerformed

    private void TelaCategoriasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TelaCategoriasActionPerformed
        cardLayout.show (PainelPrincipal, "Painel1");
    }//GEN-LAST:event_TelaCategoriasActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        Sobre sobre = new Sobre(this, true);
        sobre.setLocationRelativeTo(null);  //abre a janela centralizada na tela
        sobre.setVisible(true);
    }//GEN-LAST:event_jButton10ActionPerformed

    private void SalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalvarActionPerformed
        try {
            System.out.println(Categorias.caminhoDoArquivo.getToolTipText());
            Arquivo.salvar(Categorias.caminhoDoArquivo.getToolTipText(), Categorias.taTextoArquivo.getText(), false);
        } catch (IOException ex) {
            Logger.getLogger(Categorias.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_SalvarActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        NovoActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        AbrirActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
//        cardLayout.show (PainelPrincipal, "Painel2");
        trocaPainel(2);
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Abrir;
    private javax.swing.JButton Novo;
    public static javax.swing.JPanel PainelPrincipal;
    private javax.swing.JPanel PainelSuperior;
    public static javax.swing.JButton Salvar;
    private javax.swing.JButton TelaCategorias;
    private javax.swing.JButton jButton10;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenu jMenu8;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem7;
    // End of variables declaration//GEN-END:variables
    public static CardLayout cardLayout;
    private JPanel PainelDefault;
    private JPanel Painel1;
    private JPanel Painel2;
    private JPanel Painel3;
    private JPanel Painel4;
    private JPanel Painel5;
    private JPanel Painel6;
    private JPanel Painel7;
    private JPanel Painel8;
    private JPanel Painel9;
    private JPanel Painel10;
}