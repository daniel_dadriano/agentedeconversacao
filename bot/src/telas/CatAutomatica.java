
package telas;

import Arquivo.Arquivo;
import Func.GerarArquivoResposta;
import Func.NomeArquivo;
import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CatAutomatica extends javax.swing.JPanel {

    List<String> listaRespostasSemFormatacao = null;
    
    public CatAutomatica() {
        initComponents();
    }
        
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btCarregarPerguntas = new javax.swing.JButton();
        btCadastrarPerguntas = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        tfPalavraChaveSingular = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        btGerarArquivoResposta = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        tfPalavraChavePlural = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        taResposta = new javax.swing.JTextArea();

        setBorder(javax.swing.BorderFactory.createTitledBorder("Cadastro Automático"));

        btCarregarPerguntas.setText("Carregar perguntas");
        btCarregarPerguntas.setName("btCarregarPerguntas"); // NOI18N
        btCarregarPerguntas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCarregarPerguntasActionPerformed(evt);
            }
        });

        btCadastrarPerguntas.setText("Cadastrar perguntas");
        btCadastrarPerguntas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCadastrarPerguntasActionPerformed(evt);
            }
        });

        jLabel1.setText("Assunto do arquivo singular:");

        jLabel2.setText("Resposta (template)");

        btGerarArquivoResposta.setText("Gerar Arquivo");
        btGerarArquivoResposta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btGerarArquivoRespostaActionPerformed(evt);
            }
        });

        jLabel3.setText("Assunto do arquivo plural:");

        taResposta.setColumns(20);
        taResposta.setRows(5);
        jScrollPane2.setViewportView(taResposta);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tfPalavraChaveSingular)
                    .addComponent(tfPalavraChavePlural)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btGerarArquivoResposta)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btCarregarPerguntas)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btCadastrarPerguntas))
                            .addComponent(jLabel3)
                            .addComponent(jLabel2))
                        .addGap(0, 1, Short.MAX_VALUE))
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btCarregarPerguntas)
                    .addComponent(btCadastrarPerguntas))
                .addGap(32, 32, 32)
                .addComponent(jLabel1)
                .addGap(5, 5, 5)
                .addComponent(tfPalavraChaveSingular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addGap(5, 5, 5)
                .addComponent(tfPalavraChavePlural, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(btGerarArquivoResposta)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btCarregarPerguntasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCarregarPerguntasActionPerformed
        String caminho = Arquivo.abrirFileChooser();
        System.out.println(caminho);
        carregaTemplate(caminho);        
        Categorias.abreArquivo();
    }//GEN-LAST:event_btCarregarPerguntasActionPerformed

    private void btCadastrarPerguntasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCadastrarPerguntasActionPerformed
        TelaPrincipal.trocaPainel(2);
    }//GEN-LAST:event_btCadastrarPerguntasActionPerformed

    private void btGerarArquivoRespostaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btGerarArquivoRespostaActionPerformed
        gerarArquivoResposta();
    }//GEN-LAST:event_btGerarArquivoRespostaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btCadastrarPerguntas;
    private javax.swing.JButton btCarregarPerguntas;
    private javax.swing.JButton btGerarArquivoResposta;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea taResposta;
    private javax.swing.JTextField tfPalavraChavePlural;
    private javax.swing.JTextField tfPalavraChaveSingular;
    // End of variables declaration//GEN-END:variables

    public void carregaTemplate(String caminho){

        String texto = null;
        
        try {
            texto = Arquivo.ler(caminho);
            listaRespostasSemFormatacao = Arquivo.lerArquivo(caminho);           
            Categorias.caminhoDoArquivo.setText(NomeArquivo.NomeArquivo(caminho));
            Categorias.caminhoDoArquivo.setForeground(Color.gray);
            Categorias.caminhoDoArquivo.setToolTipText(caminho);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CatAutomatica.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CatAutomatica.class.getName()).log(Level.SEVERE, null, ex);
        }
        Categorias.taTextoArquivo.setText(texto);
    }
    
    private void gerarArquivoResposta(){
                
        if (listaRespostasSemFormatacao != null){
            GerarArquivoResposta gerarRepostas = new GerarArquivoResposta(listaRespostasSemFormatacao);
            Categorias.taTextoArquivo.setText("");            
            
            List<String> lista = gerarRepostas.gerarArquivoAiml(tfPalavraChaveSingular.getText(), tfPalavraChavePlural.getText(), taResposta.getText());
            String arquivoResposta = "";
            
            for (String texto : lista){
                arquivoResposta = arquivoResposta + texto;                
            }            
            
            Categorias.taTextoArquivo.setText(arquivoResposta);
        }
        
        
    }

}
